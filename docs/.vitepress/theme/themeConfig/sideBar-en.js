export default {
  '/en/components/': [
    { text: 'Theme', link: '/en/components/theme' },
    {
      text: 'General',
      items: [{ text: 'Introduction', link: '/en/components/introduction' }],
    },
    {
      text: 'Layout',
      items: [
        { text: 'Header', link: '/en/components/header' },
        { text: 'Layout', link: '/en/components/layout' },
      ],
    },
    {
      text: 'Conversation',
      items: [{ text: 'Bubble', link: '/en/components/bubble' }],
    },
    {
      text: 'Input',
      items: [
        { text: 'Input', link: '/en/components/input' },
        { text: 'Prompt', link: '/en/components/prompt' },
        { text: 'Mention', link: '/en/components/mention' },
      ],
    },
    {
      text: 'Developing',
      items: [
        { text: 'MarkDown Card', link: '/components/markDownCard/demo' }
      ],
    },
  ],
  '/en/design/': [
    { text: 'Intro', link: '/en/design/intro' },
    {
      text: 'Basic',
      items: [
        { text: 'Color', link: '/en/design/color' },
        { text: 'Font', link: '/en/design/font' },
      ],
    },
  ],
  '/en/use-guide/': [
    { text: 'Start', link: '/en/use-guide/introduction' },
    { text: 'i18n', link: '/use-guide/i18n' },
  ],
  '/en/playground/': [{ text: 'Demo', link: '/en/playground/playground' }],
};
